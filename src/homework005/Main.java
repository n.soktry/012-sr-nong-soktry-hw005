package homework005;

public class Main {
    public static void main(String[] args) {
        MyThread threadGreet = new MyThread("Hello KSHRD!");
        MyThread threadStar = new MyThread("*************************************");
        MyThread threadSpeech = new MyThread("I will try my best to be here at HRD.");
        MyThread threadhyphen = new MyThread("-------------------------------------");
        MyThread threadDownload = new MyThread(".........");
        try {
            System.out.println("=> Greeting: java Greeting");
            threadGreet.start();
            threadGreet.join();
            threadStar.start();
            threadStar.join();
            threadSpeech.start();
            threadSpeech.join();
            threadhyphen.start();
            threadhyphen.join();
            System.out.print("Downloading ");
            threadDownload.start();
            threadDownload.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MyThread extends Thread {
    public String msg;

    //    constructor
    MyThread(String msg) {
        this.msg = msg;
    }

    @Override
    public void run() {
        char[] chars = msg.toCharArray();
//        while loop through the msg
        int i = 0;
        while (i < msg.length()) {
            System.out.print(chars[i]);
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
//        check if (.) is already 9 times = 90%
//        the last is completed 100%
        if (msg.contains(".........")) {
            System.out.print(" Completed 100%!");
        }
        System.out.println();
    }

}
